module AgileCrm
  module Services
    module PatchContactHelpers
      # API Helper extracts out api related CRUD actions

      def update_amount(customer_deposit_product)
        current_agile_deal = @api_client.get_deal(customer_deposit_product.agile_crm_deal_id)
        if current_agile_deal && current_agile_deal['expected_value'] != customer_deposit_product.amount
          @api_client.update_deal(current_agile_deal['id'],
                                 name: current_agile_deal['name'],
                                 contact_ids: current_agile_deal['contact_ids'],
                                 milestone: current_agile_deal['milestone'],
                                 expected_value: customer_deposit_product.amount
          )
        end
      rescue StandardError => err
        Rails.logger.error(err)
      end

      def update_agile_crm_contact(customer)
        @api_client.update_contact(
            customer.agile_crm_contact_id,
            email: customer.email,
            first_name: customer.first_name,
            last_name: customer.last_name,
            phone: { mobile: customer.mobile_phone_number.to_s, home: customer.phone_number.to_s },
            address: { postal: customer.decorate.postal_address.to_s },
            date_of_birth: customer.date_of_birth.to_time.to_i
        )
      rescue StandardError => err
        Rails.logger.error(err)
      end

      def create_agile_crm_contact(customer)
        res = @api_client.create_contact(
            email: customer.email,
            first_name: customer.first_name,
            last_name: customer.last_name,
            phone: { mobile: customer.mobile_phone_number.to_s, home: customer.phone_number.to_s },
            address: { postal: customer.decorate.postal_address.to_s },
            date_of_birth: customer.date_of_birth.to_time.to_i
        )
        customer.update_attribute(:agile_crm_contact_id, res['id']) if res
      rescue StandardError => err
        Rails.logger.error(err)
      end

      def create_deal(customer_deposit_product)
        res = @api_client.create_deal(deal_attributes(customer_deposit_product))
        customer_deposit_product.update_attribute(:agile_crm_deal_id, res['id']) if res
      rescue StandardError => err
        Rails.logger.error(err)
      end

      def update_deal(customer_deposit_product)
        @api_client.update_deal(customer_deposit_product.agile_crm_deal_id, deal_attributes(customer_deposit_product))
      rescue StandardError => err
        Rails.logger.error(err)
      end

    end
  end
end