module AgileCrm
  module Services
    module PatchContactHelpers
      # Customer Helper extracts out customer model related function

      def specific_customers(list_of_customers)
        customers_relations.find(list_of_customers).select { |c| valid_customer?(c) }.each do |customer|
          process_customer customer
        end
      end

      def customers_with_limitation(limit)
        customers_relations.limit(limit).select { |c| valid_customer?(c) }.each do |customer|
          process_customer customer
        end
      end

      def customers_without_limitaion
        selected_customers = 0
        customers_relations.find_in_batches(batch_size: 100) do |batch|
          customers = batch.select { |c| valid_customer?(c) }
          selected_customers += customers.count
          customers.each do |customer|
            process_customer customer
          end
        end
        selected_customers
      end

      def customers
        Customer.includes(customer_deposit_products: [:customer, :deposit_product, procedures: :party_procedures]).find_in_batches(batch_size: 10) do |batch|
          batch.select { |c| valid_customer?(c) }
        end
      end

      def customers_relations
        Customer.includes(customer_deposit_products: [:customer, :deposit_product, procedures: :party_procedures])
      end

      def process_customer(customer)
        Rails.logger.info("processing Customer No. #{ customer.id } with email #{ customer.email }. \n")

        if customer.agile_crm_contact_id.present?
          update_agile_crm_contact(customer)
          update_exist_customer_deals(customer)
        else
          create_agile_crm_contact(customer)
          create_new_customer_deals(customer)
        end
      end

      def update_exist_customer_deals(customer)
        customer.customer_deposit_products.each do |customer_deposit_product|
          if customer_deposit_product.agile_crm_deal_id.present?
            if milestone_name(customer_deposit_product) == 'target_bank_funds_received'
              update_deal(customer_deposit_product)
            else
              update_amount(customer_deposit_product)
            end
          else
            create_deal(customer_deposit_product)
          end
        end
      end

      def deal_attributes(customer_deposit_product)
        deposit_name = customer_deposit_product.deposit_product.decorate.customer_friendly_description_decorated
        {
            name: deposit_name,
            contact_ids: [customer_deposit_product.customer.agile_crm_contact_id],
            milestone: Milestones[milestone_name(customer_deposit_product)],
            expected_value: customer_deposit_product.amount.to_i
        }
      end

      def milestone_name(customer_deposit_product)
        return 'soft_lead' unless customer_deposit_product.confirmed.present?

        procedure = customer_deposit_product.procedures.first
        party_procedures = procedure.party_procedures

        source_party = party_procedure(party_procedures, 'source_party')
        target_party = party_procedure(party_procedures, 'target_party')

        if target_party[:state] == 'completed'
          'target_bank_funds_received'
        else
          source_party_milestone(source_party[:state], customer_deposit_product)
        end
      end

      # rubocop:disable CyclomaticComplexity, PerceivedComplexity
      def source_party_milestone(state, customer_deposit_product)
        type = customer_deposit_product.procedures.first.kind
        case state
          when 'waiting_for_customer_application'
            'pdf_downloaded'
          when 'receiving'
            receiving_state(customer_deposit_product, type)
          when 'resolving'
            'problems'
          when 'problems', 'corrections', 'bank_account', 'docs_uploading', 'welcome_package', 'entax'
            'documents_received'
          when 'money_receiving'
            type == 'ncd' ? 'welcome_pack_sent' : 'documents_received'
          when 'target_bank_info', 'funds_transfer', 'funds_transfer_info', 'completed'
            'source_bank_funds_received'
          else
            fail("Undefined milestone for #{ state } state for customer deposit product No. #{ customer_deposit_product.id }")
        end
      end

      def create_new_customer_deals(customer)
        customer.customer_deposit_products.each do |customer_deposit_product|
          create_deal(customer_deposit_product)
        end
      end

      def valid_customer?(customer)
        # customer has not submitted step 1, if first_name is missing
        return false unless customer.first_name.present?
        return false if customer.status == 'terminated'

        values = [customer.first_name, customer.last_name, customer.email]
        !values.any? { |val| val =~ /test/i }
      end

      def receiving_state(customer_deposit_product, type)
        country = customer_deposit_product.deposit_product.country
        if type == 'ecd' || type == 'ncd' && country != 'AT'
          'unqualified_lead'
        elsif customer_deposit_product.pdf_download_date.present? || type == 'ncd' && country == 'AT'
          'pdf_downloaded'
        end
      end

      def party_procedure(procedures, type)
        procedures.detect { |procedure| procedure[:party_role] == type }
      end


    end
  end
end