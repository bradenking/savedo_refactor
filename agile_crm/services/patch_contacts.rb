require 'patch_contact_helpers/api_helper'
require 'patch_contact_helpers/customer_helper'

# rubocop:disable ClassLength

module AgileCrm
  module Services
    # PatchContacts is responsible of patching all of our customers with their deal into agile crm
    class PatchContacts

      # Inject an instance of the api_client
      def initialize(api_client)
        @api_client = api_client
      end

      # rubocop:disable MethodLength
      def execute(limit = 0, list_of_customers = [])
        return specific_customers(list_of_customers) if list_of_customers.present?
        if limit.to_i > 0
          customers_with_limitation limit
        else
          customers_without_limitaion
        end
      end
      
    end
  end
end