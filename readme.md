# Savedo Refactoring Task

Upon initial inspection of the supplied class meant for refactoring, it was an intimidating class of text making it hard
to quickly identify it's function and would therefore result in a maintenance nightmare. In the interest of producing
clean and maintainable code, I refactored the code based on the following principles and ideas.

## SRP

In the spirit of the Single Responsibility Principle, I split the code into the high-level command (initialize & execute),
an API helper responsible for handling the api_client CRUD operations and a customer helper which is responsible for the
customer model CRUD operations.

## DI

By injecting the api_client into the patch_contacts constructor, this is in line with the Dependency Inversion Principle.
This promotes easy testing and allows for the api_client implementation to be swapped out with ease.